///generate_other(number_to_generate,max_size,max_random,tile_type);

for(var i = 0; i < argument0; i++){ //Generate 3 ponds.
    tile_x = irandom(120);
    tile_y = irandom(120);
    h_size = irandom(argument2) + argument1;
    v_size = irandom(argument2) + argument1;
    for(var h = 0; h < h_size * v_size; h++){
        make_tile(tile_x + floor(h/h_size),tile_y + h mod h_size,argument3);
    }
}
