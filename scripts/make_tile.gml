#define make_tile
///make_tile(x,y,type)
if(position_empty(argument0,argument1) && (argument0 < 120 && argument1 < 120)){
    var tile = instance_create(argument0,argument1, obj_terrain);
    tile.image_blend = argument2;
}

#define make_bot
///make_tile(x,y,color)
var bot = instance_create(argument0,argument1, obj_bot);
bot.image_blend = argument2;
